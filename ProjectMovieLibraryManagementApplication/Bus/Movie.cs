﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectMovieLibraryManagementApplication.Bus
{
    enum EnumMovieType
    {
        None, Action, Adventure, Comedy, CrimeAndCangster, Drama, EpicsOrHistorical, Horror, MusicalsOrDance, ScienceFiction, War, Westerns
    }

    enum EnumMovieLanguage
    {
       None, English, Canadian, French, Australian, German, Indian, Russian, Korean, Mexican, Thai, Japanese, Chinese, Others
    }

    enum EnumMovieCountry
    {
        None, American, Canada, Frence, Australia, Germany, India, Russia, Korea, Mexico, Thailand, Japan, China, Others
    }
    [Serializable]
    class Time
    {
        private int hour;
        private int minute;
        private int second;

        public int Hour { get => hour; set => hour = value; }
        public int Minute { get => minute; set => minute = value; }
        public int Second { get => second; set => second = value; }

        public Time()
        { }

        public Time(int hour, int inute, int second)
        {
            this.Hour = hour;
            this.Minute = minute;
            this.Second = second;
        }
        override
        public string ToString()
        {
            return this.Hour + "-" + this.Minute + "-" + this.Second;
        
        }
    }

    [Serializable]
    class Movie
    {
        private string movieTitle;
        private string director;
        private EnumMovieLanguage language;
        private EnumMovieCountry country;
        private DateTime releaseDate;
        private EnumMovieType type;
        private Time duration;

        public string MovieTitle { get => movieTitle; set => movieTitle = value; }
        public string Director { get => director; set => director = value; }
        public DateTime ReleaseDate { get => releaseDate; set => releaseDate = value; }
        internal EnumMovieType Type { get => type; set => type = value; }
        internal Time Duration { get => duration; set => duration = value; }
        internal EnumMovieLanguage Language { get => language; set => language = value; }
        internal EnumMovieCountry Country { get => country; set => country = value; }

        public Movie()
        {
            
        }

        public Movie(string movieTitle, string director, EnumMovieLanguage language, EnumMovieCountry country, DateTime releaseDate, EnumMovieType type, Time duration)
        {
            this.MovieTitle = movieTitle;
            this.Director = director;
            this.Language = language ;
            this.Country = country ;
            this.ReleaseDate = releaseDate;
            this.Type = type;
            this.Duration = duration;
        }

        override
            public string ToString()
        {
            string output;
            output = this.MovieTitle + "\t" + this.Director + "\t\t" + this.Language + "\t\t" + this.Country + "\t\t"  + this.ReleaseDate + "\t" + this.Type+ "\t" + this.Duration;
            return output;
        }


    }
}
