﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjectMovieLibraryManagementApplication.Bus;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml;
using System.Xml.Serialization;

namespace ProjectMovieLibraryManagementApplication
{
    public partial class MovieLibraryForm : Form
    {
        List<Movie> myMovieList = new List<Movie>();
        int index;

        static String txtFilepath = @"../../Data/MovieInfos.txt";
        static String binFilepath = @"../../Data/MovieBinary.bin";

        public MovieLibraryForm()
        {
            InitializeComponent();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            Time aTime = new Time();
            aTime.Hour = Convert.ToInt32(this.textBoxHour.Text);
            aTime.Minute  = Convert.ToInt32(this.textBoxMinute.Text);
            aTime.Second  = Convert.ToInt32(this.textBoxSecond.Text);

            Movie aMovie = new Movie();
            string movieTitle; string director; EnumMovieLanguage language; EnumMovieCountry country; EnumMovieType type;
            movieTitle = Convert.ToString(textBoxMovieTitle.Text);
            director = Convert.ToString(textBoxDirector.Text);
            language  = (EnumMovieLanguage)comboBoxLanguage.SelectedIndex;
            country = (EnumMovieCountry)comboBoxCountry.SelectedIndex;
            type = (EnumMovieType)comboBoxType.SelectedIndex;

            aMovie.MovieTitle = movieTitle;
            aMovie.Director = director;
            aMovie.Language = language;
            aMovie.Country = country;
            aMovie.Duration = aTime;
            aMovie.Type = type;
            aMovie.ReleaseDate = this.dateTimePickerReleaseDate.Value;
           
            this.myMovieList.Add(aMovie);
        }

        

        private void buttonRemove_Click(object sender, EventArgs e)
        {

           
            this.myMovieList.RemoveAt(index);
            this.listBoxMovie.Items.Clear();

            foreach (Movie aMovie in this.myMovieList)
            {
                this.listBoxMovie.Items.Add(aMovie);
            }

        }

        private void buttonModify_Click(object sender, EventArgs e)
        {
            DialogResult Result = MessageBox.Show("Do you want really to update this record ? ",
                                                   "UPDATE",
                                                    MessageBoxButtons.YesNo,
                                                    MessageBoxIcon.Question);

            if (Result == DialogResult .Yes )
         {
            this.myMovieList[index].MovieTitle = Convert.ToString(textBoxMovieTitle.Text);
            this.myMovieList[index].Director = Convert.ToString(textBoxDirector.Text);
            this.myMovieList[index].Language  = (EnumMovieLanguage)comboBoxLanguage.SelectedIndex;
            this.myMovieList[index].Country = (EnumMovieCountry)comboBoxCountry.SelectedIndex;
            this.myMovieList[index].Type = (EnumMovieType)comboBoxType.SelectedIndex;
            this.myMovieList[index].ReleaseDate = this.dateTimePickerReleaseDate.Value;
            
          
            MessageBox.Show("...record undated...");
            this.listBoxMovie.Items.Clear();
            foreach (Movie element in this.myMovieList)
            {
                    this.listBoxMovie.Items.Add(element);
            }
            }
            else { MessageBox.Show("...record NOT updated...", "UPDATE"); }
        }

        private void buttonsearch_Click(object sender, EventArgs e)
        {
            bool found = false;
            Movie theMovie = new Movie();

            foreach (Movie  element in this.myMovieList )
            {
                if (element.MovieTitle  == Convert.ToString (textBoxSearchInfos.Text))
                {
                    found = true;

                    theMovie = element;

                }
            }
            if (found)
            {
                MessageBox.Show("..Account Found..." + theMovie.ToString ());
            }
            else
            {
                MessageBox.Show("..Account NOT Found...");
            }
        }

        private void listBoxMovie_SelectedIndexChanged(object sender, EventArgs e)
        {
            index = listBoxMovie.SelectedIndex;
            MessageBox.Show("selected index: " + index);
            textBoxMovieTitle.Text = Convert.ToString(this.myMovieList[index].MovieTitle );
            textBoxDirector.Text = Convert.ToString(this.myMovieList[index].Director );
            comboBoxLanguage.Text = Convert.ToString(this.myMovieList[index].Language  );
            comboBoxCountry.Text = Convert.ToString(this.myMovieList[index].Country );
            comboBoxType.Text =Convert.ToString ( this.myMovieList [index].Type );
            this.dateTimePickerReleaseDate.Value = this.myMovieList [index].ReleaseDate;
          
        }

        private void MovieLibraryForm_Load(object sender, EventArgs e)
        {
            foreach (EnumMovieType element in Enum.GetValues(typeof(EnumMovieType )))
            {
                comboBoxType.Items.Add(element);
            }
            comboBoxType.Text = Convert.ToString(comboBoxType.Items[0]);

            foreach (EnumMovieCountry element in Enum.GetValues(typeof(EnumMovieCountry)))
            {
                comboBoxCountry.Items.Add(element);
            }
            comboBoxCountry.Text = Convert.ToString(comboBoxCountry.Items[0]);


            foreach (EnumMovieLanguage element in Enum.GetValues(typeof(EnumMovieLanguage)))
            {
                comboBoxLanguage.Items.Add(element);
            }
            comboBoxLanguage.Text = Convert.ToString(comboBoxLanguage.Items[0]);

        }

        private void listViewMovie_SelectedIndexChanged(object sender, EventArgs e)
        {
            index = listViewMovie.FocusedItem.Index;

            textBoxMovieTitle.Text = Convert.ToString(myMovieList[index].MovieTitle);
            textBoxDirector.Text = Convert.ToString(myMovieList[index].Director);
            comboBoxLanguage.Text = Convert.ToString(myMovieList[index].Language );
            comboBoxCountry.Text = Convert.ToString(myMovieList[index].Country);
            textBoxHour.Text = Convert.ToString(myMovieList[index].Duration.Hour);
            textBoxMinute.Text = Convert.ToString(myMovieList[index].Duration.Minute);
            textBoxSecond.Text = Convert.ToString(myMovieList[index].Duration.Second);
            dateTimePickerReleaseDate.Value = DateTime.Parse(Convert.ToString(myMovieList[index].ReleaseDate));
            comboBoxType.Text = Convert.ToString(myMovieList[index].Type);
           
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonDisplay_Click(object sender, EventArgs e)
        {
            listViewMovie.Items.Clear();
            listBoxMovie.Items.Clear();
            if (this.myMovieList.Capacity != 0)
            {
                foreach (Movie item in this.myMovieList)
                {
                    this.listBoxMovie.Items.Add(item);
                }
            }
            else
            {
                MessageBox.Show("...NO DATA...");
            }

            foreach (Movie element in myMovieList)
            {
                ListViewItem item = new ListViewItem(Convert.ToString(element.MovieTitle));
                   
                item.SubItems.Add(Convert.ToString(element.Director));
                item.SubItems.Add(Convert.ToString(element.Language));
                item.SubItems.Add(Convert.ToString(element.Country));
                item.SubItems.Add(Convert.ToString(element.Duration));
                item.SubItems.Add(Convert.ToString(element.ReleaseDate));
                item.SubItems.Add(Convert.ToString(element.Type));

                this.listViewMovie.Items.Add(item);
            }
        }

        private void buttonWTTF_Click(object sender, EventArgs e)
        {
            using (StreamWriter writer = File.CreateText(txtFilepath))
            {
                foreach (Movie aMovie in this.myMovieList )
                {
                    writer.WriteLine(aMovie.MovieTitle + "|" + aMovie.Director + "|" +aMovie .Language + "|" + aMovie .Country + "|" + aMovie.Duration.ToString () +  "|" + aMovie .ReleaseDate + "|" + aMovie .Type  );
                }
            }
            
        }

        private void buttonRFTF_Click(object sender, EventArgs e)
        {
            myMovieList.Clear();
            this.listBoxMovie.Items.Clear();
            this.listViewMovie.Items.Clear();
            string duration;

            StreamReader reader = new StreamReader(txtFilepath);
            string[] lines = System.IO.File.ReadAllLines(txtFilepath);

            foreach (string line1 in lines)
            {
                Movie amovie = new Movie();

                string[] tokens = line1.Split('|');
                amovie.MovieTitle = Convert.ToString(tokens[0]);
                amovie.Director = Convert.ToString(tokens[1]);
                amovie.Language = (EnumMovieLanguage)Enum.Parse(typeof(EnumMovieLanguage), tokens[2]);
                amovie.Country = (EnumMovieCountry)Enum.Parse(typeof(EnumMovieCountry), tokens[3]);
                
                duration = tokens[4];
                String[] tokenstime = duration.Split('-');
                Time atime = new Time();
                atime.Hour = Convert.ToInt32(tokenstime[0]);
                atime.Minute = Convert.ToInt32(tokenstime[1]);
                atime.Second = Convert.ToInt32(tokenstime[2]);
                amovie.Duration = atime;

                amovie.ReleaseDate = DateTime.Parse(tokens[5]);
                amovie.Type = (EnumMovieType)Enum.Parse(typeof(EnumMovieType), tokens[6]);

                myMovieList.Add(amovie);

                foreach (Movie element in myMovieList)
                {
                    this.listBoxMovie.Items.Add(element);

                    ListViewItem item = new ListViewItem(Convert.ToString(element.MovieTitle));
                    item.SubItems.Add(Convert.ToString(element.Director));
                    item.SubItems.Add(Convert.ToString(element.Language));
                    item.SubItems.Add(Convert.ToString(element.Country));
                    item.SubItems.Add(Convert.ToString(element.Duration));
                    item.SubItems.Add(Convert.ToString(element.ReleaseDate));
                    item.SubItems.Add(Convert.ToString(element.Type));

                    this.listViewMovie.Items.Add(item);
                }
            }

        }

        private void buttonReset_Click(object sender, EventArgs e)
        {
            textBoxMovieTitle.Text = "";
            textBoxDirector.Text = "";
            textBoxHour.Text = "";
            textBoxMinute.Text = "";
            textBoxSecond.Text = "";
            textBoxSearchInfos.Text = "";
            comboBoxLanguage.Text = Convert.ToString(EnumMovieLanguage.None);
            comboBoxCountry.Text = Convert.ToString(EnumMovieCountry.None);
            comboBoxType.Text = Convert.ToString(EnumMovieType.None);
            dateTimePickerReleaseDate.Value = DateTime.Parse(Convert.ToString(myMovieList[index].ReleaseDate));

            textBoxMovieTitle.Focus();
            listBoxMovie.Items.Clear();
            listViewMovie.Items.Clear();
            
        }

        private void buttonRTBF_Click(object sender, EventArgs e)
        {
            FileStream fs = new FileStream(binFilepath, FileMode.OpenOrCreate, FileAccess.Write);
            BinaryFormatter writer = new BinaryFormatter();
            writer.Serialize(fs, myMovieList);
            fs.Close();
        }

        private void buttonRFBF_Click(object sender, EventArgs e)
        {
            if(File.Exists(binFilepath))
            {
                FileStream fs = new FileStream(binFilepath, FileMode.Open, FileAccess.Read);
                BinaryFormatter bin = new BinaryFormatter();

                myMovieList = (List<Movie>)bin.Deserialize(fs);
                fs.Close();
            }
            foreach(Movie element in myMovieList )
            {
                this.listBoxMovie.Items.Add(element);
                ListViewItem item = new ListViewItem(Convert.ToString(element.MovieTitle));

                item.SubItems.Add(Convert.ToString(element.Director));
                item.SubItems.Add(Convert.ToString(element.Language));
                item.SubItems.Add(Convert.ToString(element.Country));
                item.SubItems.Add(Convert.ToString(element.Duration));
                item.SubItems.Add(Convert.ToString(element.ReleaseDate));
                item.SubItems.Add(Convert.ToString(element.Type));

                this.listViewMovie.Items.Add(item);
              
            }
        }

        
    }
}

