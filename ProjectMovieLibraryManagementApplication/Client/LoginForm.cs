﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectMovieLibraryManagementApplication
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            if ((this.textBoxUserName.Text == "admin")  && ( this.textBoxPassword.Text == "123"))
            {
                MovieLibraryForm myMainForm = new MovieLibraryForm();
                myMainForm.ShowDialog();
            }
            else
            {
                MessageBox.Show("bad username or password ... try again");
            }
        }

        private void buttonReset_Click(object sender, EventArgs e)
        {
            foreach (Control myControl in Controls )
            {
                if(myControl is TextBox)
                {
                    myControl.Text = "";
                }
            }
            this.textBoxUserName.Focus();
        }

        private void buttonLogout_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textBoxUserName_KeyPress(object sender, KeyPressEventArgs e)
        {
            ValidateLetter(e);
        }



        public static void ValidateLetter(KeyPressEventArgs e)
        {
            if (!Char.IsLetter(e.KeyChar))
            {
                MessageBox.Show(" Must be a letter only \n Message from ValidateLetter operation.");
                e.Handled = true;
            }
        }

        private void textBoxPassword_KeyPress(object sender, KeyPressEventArgs e)
        {
            ValidateDigit(e);
        }

        public static void ValidateDigit(KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar))
            {
                MessageBox.Show(" Must be a digit only  \n Message from ValidateDigit operation.");
                e.Handled = true;
            }
        }

        public static void ValidateDecimal(KeyPressEventArgs e)
        {
   
            if (!Char.IsDigit(e.KeyChar) && e.KeyChar != '.' && !Char.IsControl(e.KeyChar))
            {
                MessageBox.Show(" Must be a digit only and/or decimal point(.) \n Message from ValidateDigit operation.");
                e.Handled = true;
            }
        }
    }
}
