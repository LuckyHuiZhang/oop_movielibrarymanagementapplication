﻿namespace ProjectMovieLibraryManagementApplication
{
    partial class MovieLibraryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MovieLibraryForm));
            this.labelMovieTitle = new System.Windows.Forms.Label();
            this.labelDirector = new System.Windows.Forms.Label();
            this.labelLanguage = new System.Windows.Forms.Label();
            this.labelCountry = new System.Windows.Forms.Label();
            this.labelDuration = new System.Windows.Forms.Label();
            this.labelReleaseDate = new System.Windows.Forms.Label();
            this.labelType = new System.Windows.Forms.Label();
            this.textBoxMovieTitle = new System.Windows.Forms.TextBox();
            this.textBoxDirector = new System.Windows.Forms.TextBox();
            this.textBoxHour = new System.Windows.Forms.TextBox();
            this.dateTimePickerReleaseDate = new System.Windows.Forms.DateTimePicker();
            this.comboBoxType = new System.Windows.Forms.ComboBox();
            this.labelSearchInfos = new System.Windows.Forms.Label();
            this.textBoxSearchInfos = new System.Windows.Forms.TextBox();
            this.listViewMovie = new System.Windows.Forms.ListView();
            this.columnHeaderMovieTitle = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderDirector = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderLanguage = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderCountry = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderDuration = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderReleaseDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.buttonAdd = new System.Windows.Forms.Button();
            this.buttonDisplay = new System.Windows.Forms.Button();
            this.buttonRemove = new System.Windows.Forms.Button();
            this.buttonModify = new System.Windows.Forms.Button();
            this.buttonsearch = new System.Windows.Forms.Button();
            this.buttonExit = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxMinute = new System.Windows.Forms.TextBox();
            this.textBoxSecond = new System.Windows.Forms.TextBox();
            this.listBoxMovie = new System.Windows.Forms.ListBox();
            this.comboBoxCountry = new System.Windows.Forms.ComboBox();
            this.comboBoxLanguage = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.buttonWTTF = new System.Windows.Forms.Button();
            this.buttonRFTF = new System.Windows.Forms.Button();
            this.buttonRTBF = new System.Windows.Forms.Button();
            this.buttonRFBF = new System.Windows.Forms.Button();
            this.buttonReset = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelMovieTitle
            // 
            this.labelMovieTitle.AutoSize = true;
            this.labelMovieTitle.BackColor = System.Drawing.Color.Yellow;
            this.labelMovieTitle.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelMovieTitle.Location = new System.Drawing.Point(244, 31);
            this.labelMovieTitle.Name = "labelMovieTitle";
            this.labelMovieTitle.Size = new System.Drawing.Size(73, 15);
            this.labelMovieTitle.TabIndex = 0;
            this.labelMovieTitle.Text = "Movie Title   :";
            // 
            // labelDirector
            // 
            this.labelDirector.AutoSize = true;
            this.labelDirector.BackColor = System.Drawing.Color.Yellow;
            this.labelDirector.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelDirector.Location = new System.Drawing.Point(32, 67);
            this.labelDirector.Name = "labelDirector";
            this.labelDirector.Size = new System.Drawing.Size(76, 15);
            this.labelDirector.TabIndex = 1;
            this.labelDirector.Text = "Director         :";
            // 
            // labelLanguage
            // 
            this.labelLanguage.AutoSize = true;
            this.labelLanguage.BackColor = System.Drawing.Color.Yellow;
            this.labelLanguage.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelLanguage.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelLanguage.Location = new System.Drawing.Point(473, 95);
            this.labelLanguage.Name = "labelLanguage";
            this.labelLanguage.Size = new System.Drawing.Size(63, 15);
            this.labelLanguage.TabIndex = 2;
            this.labelLanguage.Text = "Language :";
            // 
            // labelCountry
            // 
            this.labelCountry.AutoSize = true;
            this.labelCountry.BackColor = System.Drawing.Color.Yellow;
            this.labelCountry.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelCountry.Location = new System.Drawing.Point(471, 69);
            this.labelCountry.Name = "labelCountry";
            this.labelCountry.Size = new System.Drawing.Size(63, 15);
            this.labelCountry.TabIndex = 3;
            this.labelCountry.Text = "Country     :";
            // 
            // labelDuration
            // 
            this.labelDuration.AutoSize = true;
            this.labelDuration.BackColor = System.Drawing.Color.Yellow;
            this.labelDuration.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelDuration.Location = new System.Drawing.Point(33, 125);
            this.labelDuration.Name = "labelDuration";
            this.labelDuration.Size = new System.Drawing.Size(76, 15);
            this.labelDuration.TabIndex = 4;
            this.labelDuration.Text = "Duration        :";
            // 
            // labelReleaseDate
            // 
            this.labelReleaseDate.AutoSize = true;
            this.labelReleaseDate.BackColor = System.Drawing.Color.Yellow;
            this.labelReleaseDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelReleaseDate.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.labelReleaseDate.Location = new System.Drawing.Point(32, 95);
            this.labelReleaseDate.Name = "labelReleaseDate";
            this.labelReleaseDate.Size = new System.Drawing.Size(77, 15);
            this.labelReleaseDate.TabIndex = 5;
            this.labelReleaseDate.Text = "ReleaseDate :";
            // 
            // labelType
            // 
            this.labelType.AutoSize = true;
            this.labelType.BackColor = System.Drawing.Color.Yellow;
            this.labelType.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelType.Location = new System.Drawing.Point(473, 122);
            this.labelType.Name = "labelType";
            this.labelType.Size = new System.Drawing.Size(63, 15);
            this.labelType.TabIndex = 6;
            this.labelType.Text = "Type         :";
            // 
            // textBoxMovieTitle
            // 
            this.textBoxMovieTitle.Location = new System.Drawing.Point(336, 28);
            this.textBoxMovieTitle.Name = "textBoxMovieTitle";
            this.textBoxMovieTitle.Size = new System.Drawing.Size(200, 20);
            this.textBoxMovieTitle.TabIndex = 7;
            // 
            // textBoxDirector
            // 
            this.textBoxDirector.Location = new System.Drawing.Point(122, 67);
            this.textBoxDirector.Name = "textBoxDirector";
            this.textBoxDirector.Size = new System.Drawing.Size(200, 20);
            this.textBoxDirector.TabIndex = 8;
            // 
            // textBoxHour
            // 
            this.textBoxHour.Location = new System.Drawing.Point(122, 122);
            this.textBoxHour.Name = "textBoxHour";
            this.textBoxHour.Size = new System.Drawing.Size(52, 20);
            this.textBoxHour.TabIndex = 11;
            // 
            // dateTimePickerReleaseDate
            // 
            this.dateTimePickerReleaseDate.Location = new System.Drawing.Point(122, 95);
            this.dateTimePickerReleaseDate.Name = "dateTimePickerReleaseDate";
            this.dateTimePickerReleaseDate.Size = new System.Drawing.Size(200, 20);
            this.dateTimePickerReleaseDate.TabIndex = 12;
           
            // 
            // comboBoxType
            // 
            this.comboBoxType.FormattingEnabled = true;
            this.comboBoxType.Location = new System.Drawing.Point(559, 119);
            this.comboBoxType.Name = "comboBoxType";
            this.comboBoxType.Size = new System.Drawing.Size(200, 21);
            this.comboBoxType.TabIndex = 13;
            // 
            // labelSearchInfos
            // 
            this.labelSearchInfos.AutoSize = true;
            this.labelSearchInfos.BackColor = System.Drawing.Color.Yellow;
            this.labelSearchInfos.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelSearchInfos.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.labelSearchInfos.Location = new System.Drawing.Point(244, 163);
            this.labelSearchInfos.Name = "labelSearchInfos";
            this.labelSearchInfos.Size = new System.Drawing.Size(78, 15);
            this.labelSearchInfos.TabIndex = 14;
            this.labelSearchInfos.Text = "Search Infos  :";
            // 
            // textBoxSearchInfos
            // 
            this.textBoxSearchInfos.Location = new System.Drawing.Point(346, 158);
            this.textBoxSearchInfos.Name = "textBoxSearchInfos";
            this.textBoxSearchInfos.Size = new System.Drawing.Size(200, 20);
            this.textBoxSearchInfos.TabIndex = 15;
            // 
            // listViewMovie
            // 
            this.listViewMovie.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderMovieTitle,
            this.columnHeaderDirector,
            this.columnHeaderLanguage,
            this.columnHeaderCountry,
            this.columnHeaderDuration,
            this.columnHeaderReleaseDate,
            this.columnHeaderType});
            this.listViewMovie.GridLines = true;
            this.listViewMovie.Location = new System.Drawing.Point(12, 393);
            this.listViewMovie.Name = "listViewMovie";
            this.listViewMovie.Size = new System.Drawing.Size(763, 163);
            this.listViewMovie.TabIndex = 17;
            this.listViewMovie.UseCompatibleStateImageBehavior = false;
            this.listViewMovie.View = System.Windows.Forms.View.Details;
            this.listViewMovie.SelectedIndexChanged += new System.EventHandler(this.listViewMovie_SelectedIndexChanged);
            // 
            // columnHeaderMovieTitle
            // 
            this.columnHeaderMovieTitle.Text = "Movie Title";
            this.columnHeaderMovieTitle.Width = 122;
            // 
            // columnHeaderDirector
            // 
            this.columnHeaderDirector.Text = "Director";
            this.columnHeaderDirector.Width = 81;
            // 
            // columnHeaderLanguage
            // 
            this.columnHeaderLanguage.Text = "Language";
            this.columnHeaderLanguage.Width = 87;
            // 
            // columnHeaderCountry
            // 
            this.columnHeaderCountry.Text = "Country";
            this.columnHeaderCountry.Width = 83;
            // 
            // columnHeaderDuration
            // 
            this.columnHeaderDuration.Text = "Duration";
            this.columnHeaderDuration.Width = 103;
            // 
            // columnHeaderReleaseDate
            // 
            this.columnHeaderReleaseDate.Text = "ReleaseDate";
            this.columnHeaderReleaseDate.Width = 155;
            // 
            // columnHeaderType
            // 
            this.columnHeaderType.Text = "Type";
            this.columnHeaderType.Width = 280;
            // 
            // buttonAdd
            // 
            this.buttonAdd.BackColor = System.Drawing.SystemColors.Highlight;
            this.buttonAdd.Location = new System.Drawing.Point(12, 579);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(75, 23);
            this.buttonAdd.TabIndex = 18;
            this.buttonAdd.Text = "Add";
            this.buttonAdd.UseVisualStyleBackColor = false;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // buttonDisplay
            // 
            this.buttonDisplay.BackColor = System.Drawing.SystemColors.Highlight;
            this.buttonDisplay.Location = new System.Drawing.Point(164, 579);
            this.buttonDisplay.Name = "buttonDisplay";
            this.buttonDisplay.Size = new System.Drawing.Size(75, 23);
            this.buttonDisplay.TabIndex = 19;
            this.buttonDisplay.Text = "Display";
            this.buttonDisplay.UseVisualStyleBackColor = false;
            this.buttonDisplay.Click += new System.EventHandler(this.buttonDisplay_Click);
            // 
            // buttonRemove
            // 
            this.buttonRemove.BackColor = System.Drawing.SystemColors.Highlight;
            this.buttonRemove.Location = new System.Drawing.Point(560, 579);
            this.buttonRemove.Name = "buttonRemove";
            this.buttonRemove.Size = new System.Drawing.Size(75, 23);
            this.buttonRemove.TabIndex = 20;
            this.buttonRemove.Text = "Remove";
            this.buttonRemove.UseVisualStyleBackColor = false;
            this.buttonRemove.Click += new System.EventHandler(this.buttonRemove_Click);
            // 
            // buttonModify
            // 
            this.buttonModify.BackColor = System.Drawing.SystemColors.Highlight;
            this.buttonModify.Location = new System.Drawing.Point(700, 579);
            this.buttonModify.Name = "buttonModify";
            this.buttonModify.Size = new System.Drawing.Size(75, 23);
            this.buttonModify.TabIndex = 21;
            this.buttonModify.Text = "Modify";
            this.buttonModify.UseVisualStyleBackColor = false;
            this.buttonModify.Click += new System.EventHandler(this.buttonModify_Click);
            // 
            // buttonsearch
            // 
            this.buttonsearch.BackColor = System.Drawing.SystemColors.Highlight;
            this.buttonsearch.Location = new System.Drawing.Point(574, 158);
            this.buttonsearch.Name = "buttonsearch";
            this.buttonsearch.Size = new System.Drawing.Size(75, 23);
            this.buttonsearch.TabIndex = 22;
            this.buttonsearch.Text = "Search";
            this.buttonsearch.UseVisualStyleBackColor = false;
            this.buttonsearch.Click += new System.EventHandler(this.buttonsearch_Click);
            // 
            // buttonExit
            // 
            this.buttonExit.BackColor = System.Drawing.Color.Blue;
            this.buttonExit.Location = new System.Drawing.Point(12, 637);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(765, 23);
            this.buttonExit.TabIndex = 23;
            this.buttonExit.Text = "Exit";
            this.buttonExit.UseVisualStyleBackColor = false;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(180, 129);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(12, 13);
            this.label1.TabIndex = 31;
            this.label1.Text = "/";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(254, 129);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(12, 13);
            this.label2.TabIndex = 32;
            this.label2.Text = "/";
            // 
            // textBoxMinute
            // 
            this.textBoxMinute.Location = new System.Drawing.Point(198, 122);
            this.textBoxMinute.Name = "textBoxMinute";
            this.textBoxMinute.Size = new System.Drawing.Size(50, 20);
            this.textBoxMinute.TabIndex = 33;
            // 
            // textBoxSecond
            // 
            this.textBoxSecond.Location = new System.Drawing.Point(272, 122);
            this.textBoxSecond.Name = "textBoxSecond";
            this.textBoxSecond.Size = new System.Drawing.Size(50, 20);
            this.textBoxSecond.TabIndex = 34;
            // 
            // listBoxMovie
            // 
            this.listBoxMovie.FormattingEnabled = true;
            this.listBoxMovie.Location = new System.Drawing.Point(62, 229);
            this.listBoxMovie.Name = "listBoxMovie";
            this.listBoxMovie.Size = new System.Drawing.Size(632, 134);
            this.listBoxMovie.TabIndex = 35;
            // 
            // comboBoxCountry
            // 
            this.comboBoxCountry.FormattingEnabled = true;
            this.comboBoxCountry.Location = new System.Drawing.Point(559, 66);
            this.comboBoxCountry.Name = "comboBoxCountry";
            this.comboBoxCountry.Size = new System.Drawing.Size(200, 21);
            this.comboBoxCountry.TabIndex = 36;
            // 
            // comboBoxLanguage
            // 
            this.comboBoxLanguage.FormattingEnabled = true;
            this.comboBoxLanguage.Location = new System.Drawing.Point(559, 92);
            this.comboBoxLanguage.Name = "comboBoxLanguage";
            this.comboBoxLanguage.Size = new System.Drawing.Size(200, 21);
            this.comboBoxLanguage.TabIndex = 37;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(71, 204);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 38;
            this.label3.Text = "MovieTitle";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(148, 204);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 39;
            this.label4.Text = "Director";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(212, 204);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 13);
            this.label5.TabIndex = 40;
            this.label5.Text = "Language";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(423, 204);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 13);
            this.label6.TabIndex = 41;
            this.label6.Text = "ReleaseDate";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(556, 204);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(31, 13);
            this.label7.TabIndex = 42;
            this.label7.Text = "Type";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(308, 204);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(43, 13);
            this.label8.TabIndex = 43;
            this.label8.Text = "Country";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(638, 204);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 13);
            this.label9.TabIndex = 44;
            this.label9.Text = "Duration";
            // 
            // buttonWTTF
            // 
            this.buttonWTTF.BackColor = System.Drawing.SystemColors.Highlight;
            this.buttonWTTF.Location = new System.Drawing.Point(12, 608);
            this.buttonWTTF.Name = "buttonWTTF";
            this.buttonWTTF.Size = new System.Drawing.Size(144, 23);
            this.buttonWTTF.TabIndex = 45;
            this.buttonWTTF.Text = "Write To Text File";
            this.buttonWTTF.UseVisualStyleBackColor = false;
            this.buttonWTTF.Click += new System.EventHandler(this.buttonWTTF_Click);
            // 
            // buttonRFTF
            // 
            this.buttonRFTF.BackColor = System.Drawing.SystemColors.Highlight;
            this.buttonRFTF.Location = new System.Drawing.Point(244, 608);
            this.buttonRFTF.Name = "buttonRFTF";
            this.buttonRFTF.Size = new System.Drawing.Size(121, 23);
            this.buttonRFTF.TabIndex = 46;
            this.buttonRFTF.Text = "Read From Tex File";
            this.buttonRFTF.UseVisualStyleBackColor = false;
            this.buttonRFTF.Click += new System.EventHandler(this.buttonRFTF_Click);
            // 
            // buttonRTBF
            // 
            this.buttonRTBF.BackColor = System.Drawing.SystemColors.Highlight;
            this.buttonRTBF.Location = new System.Drawing.Point(443, 608);
            this.buttonRTBF.Name = "buttonRTBF";
            this.buttonRTBF.Size = new System.Drawing.Size(120, 23);
            this.buttonRTBF.TabIndex = 47;
            this.buttonRTBF.Text = "Write To Binary File";
            this.buttonRTBF.UseVisualStyleBackColor = false;
            this.buttonRTBF.Click += new System.EventHandler(this.buttonRTBF_Click);
            // 
            // buttonRFBF
            // 
            this.buttonRFBF.BackColor = System.Drawing.SystemColors.Highlight;
            this.buttonRFBF.Location = new System.Drawing.Point(641, 608);
            this.buttonRFBF.Name = "buttonRFBF";
            this.buttonRFBF.Size = new System.Drawing.Size(134, 23);
            this.buttonRFBF.TabIndex = 48;
            this.buttonRFBF.Text = "Read Form Binary File";
            this.buttonRFBF.UseVisualStyleBackColor = false;
            this.buttonRFBF.Click += new System.EventHandler(this.buttonRFBF_Click);
            // 
            // buttonReset
            // 
            this.buttonReset.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.buttonReset.Location = new System.Drawing.Point(366, 579);
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(75, 23);
            this.buttonReset.TabIndex = 49;
            this.buttonReset.Text = "Reset";
            this.buttonReset.UseVisualStyleBackColor = false;
            this.buttonReset.Click += new System.EventHandler(this.buttonReset_Click);
            // 
            // MovieLibraryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(790, 672);
            this.Controls.Add(this.buttonReset);
            this.Controls.Add(this.buttonRFBF);
            this.Controls.Add(this.buttonRTBF);
            this.Controls.Add(this.buttonRFTF);
            this.Controls.Add(this.buttonWTTF);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.comboBoxLanguage);
            this.Controls.Add(this.comboBoxCountry);
            this.Controls.Add(this.listBoxMovie);
            this.Controls.Add(this.textBoxSecond);
            this.Controls.Add(this.textBoxMinute);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonExit);
            this.Controls.Add(this.buttonsearch);
            this.Controls.Add(this.buttonModify);
            this.Controls.Add(this.buttonRemove);
            this.Controls.Add(this.buttonDisplay);
            this.Controls.Add(this.buttonAdd);
            this.Controls.Add(this.listViewMovie);
            this.Controls.Add(this.textBoxSearchInfos);
            this.Controls.Add(this.labelSearchInfos);
            this.Controls.Add(this.comboBoxType);
            this.Controls.Add(this.dateTimePickerReleaseDate);
            this.Controls.Add(this.textBoxHour);
            this.Controls.Add(this.textBoxDirector);
            this.Controls.Add(this.textBoxMovieTitle);
            this.Controls.Add(this.labelType);
            this.Controls.Add(this.labelReleaseDate);
            this.Controls.Add(this.labelDuration);
            this.Controls.Add(this.labelCountry);
            this.Controls.Add(this.labelLanguage);
            this.Controls.Add(this.labelDirector);
            this.Controls.Add(this.labelMovieTitle);
            this.Name = "MovieLibraryForm";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.MovieLibraryForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelMovieTitle;
        private System.Windows.Forms.Label labelDirector;
        private System.Windows.Forms.Label labelLanguage;
        private System.Windows.Forms.Label labelCountry;
        private System.Windows.Forms.Label labelDuration;
        private System.Windows.Forms.Label labelReleaseDate;
        private System.Windows.Forms.Label labelType;
        private System.Windows.Forms.TextBox textBoxMovieTitle;
        private System.Windows.Forms.TextBox textBoxDirector;
        private System.Windows.Forms.TextBox textBoxHour;
        private System.Windows.Forms.DateTimePicker dateTimePickerReleaseDate;
        private System.Windows.Forms.ComboBox comboBoxType;
        private System.Windows.Forms.Label labelSearchInfos;
        private System.Windows.Forms.TextBox textBoxSearchInfos;
        private System.Windows.Forms.ListView listViewMovie;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.Button buttonDisplay;
        private System.Windows.Forms.Button buttonRemove;
        private System.Windows.Forms.Button buttonModify;
        private System.Windows.Forms.Button buttonsearch;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.ColumnHeader columnHeaderMovieTitle;
        private System.Windows.Forms.ColumnHeader columnHeaderDirector;
        private System.Windows.Forms.ColumnHeader columnHeaderLanguage;
        private System.Windows.Forms.ColumnHeader columnHeaderCountry;
        private System.Windows.Forms.ColumnHeader columnHeaderDuration;
        private System.Windows.Forms.ColumnHeader columnHeaderReleaseDate;
        private System.Windows.Forms.ColumnHeader columnHeaderType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxMinute;
        private System.Windows.Forms.TextBox textBoxSecond;
        private System.Windows.Forms.ListBox listBoxMovie;
        private System.Windows.Forms.ComboBox comboBoxCountry;
        private System.Windows.Forms.ComboBox comboBoxLanguage;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button buttonWTTF;
        private System.Windows.Forms.Button buttonRFTF;
        private System.Windows.Forms.Button buttonRTBF;
        private System.Windows.Forms.Button buttonRFBF;
        private System.Windows.Forms.Button buttonReset;
    }
}

